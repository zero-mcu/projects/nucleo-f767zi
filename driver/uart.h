#ifndef __USER_UART_H__
#define __USER_UART_H__
#include <bus/uart.h>


enum {
    UART_0 = 0,
};

void user_uart_init(ze_u8_t id, uart_t* uart, ze_u32_t baudrate);

#endif