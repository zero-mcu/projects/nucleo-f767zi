#include <bus/uart.h>
#include <stm32f7xx_hal.h>
#include <stm32f7xx_ll_usart.h>
#include <stm32f7xx_ll_gpio.h>
#include "driver/uart.h"

#define UART0_INSTANCE           USART3
#define UART0_CLK_ENABLE()     __HAL_RCC_USART3_CLK_ENABLE()
#define UART0_CLK_SOURCE()     //
#define UART0_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOD_CLK_ENABLE()
#define UART0_TX_PIN               LL_GPIO_PIN_8
#define UART0_TX_GPIO_PORT              GPIOD
#define UART0_SET_TX_GPIO_AF()         LL_GPIO_SetAFPin_8_15(UART0_TX_GPIO_PORT, UART0_TX_PIN, LL_GPIO_AF_7)
#define UART0_RX_PIN               LL_GPIO_PIN_9
#define UART0_RX_GPIO_PORT              GPIOD
#define UART0_SET_RX_GPIO_AF()         LL_GPIO_SetAFPin_8_15(UART0_RX_GPIO_PORT,UART0_RX_PIN, LL_GPIO_AF_7)
#define UART0_IRQ_NUM               USART3_IRQn
#define UART0_INTERRUPT             USART3_IRQHandler

typedef struct {
    USART_TypeDef *instance;
    UART_HandleTypeDef uart_handle;
    IRQn_Type irq;
    uart_t* uart;
} stm_uart_t;

ze_u8_t uart0_rxbuf[100];

static uart_fifo_t uart0_rx_fifo;

static stm_uart_t stm_uart0 = {
    .instance = UART0_INSTANCE,
    .irq = UART0_IRQ_NUM,
};

static int uart_putchar(uart_t *uart, int ch)
{
    stm_uart_t *stm_uart;

    stm_uart = uart->private_data;

    while(!LL_USART_IsActiveFlag_TXE(stm_uart->instance));
    LL_USART_TransmitData8(stm_uart->instance, ch);
    
    return 0;
}

static int uart_getchar(uart_t* uart)
{
    stm_uart_t* stm_uart = (stm_uart_t*)uart->private_data;
    int ch = -1;

    if(LL_USART_IsActiveFlag_RXNE(stm_uart->instance))
    {
        ch = LL_USART_ReceiveData8(stm_uart->instance);
    }

    return ch;
}

const uart_ops_t uart_ops={
    .put_char = uart_putchar,
    .get_char = uart_getchar,
};


static void user_uart_config_rxfifo(uart_t* uart, uart_fifo_t* rx_fifo, ze_u8_t* buf, ze_size_t bufsz)
{
    rx_fifo->buffer = buf;
    rx_fifo->bufsz = bufsz;
    rx_fifo->get_index = 0;
    rx_fifo->put_index = 0;
    uart->uart_rx = rx_fifo;
}

static void stm_uart_init(uart_t* uart, stm_uart_t* stm_uart, ze_u32_t baudrate)
{
    UART_HandleTypeDef *huart = &stm_uart->uart_handle;
    huart->Instance = stm_uart->instance;

    huart->Init.BaudRate = baudrate;
    huart->Init.WordLength = UART_WORDLENGTH_8B;
    huart->Init.StopBits = UART_STOPBITS_1;
    huart->Init.Parity = UART_PARITY_NONE;
    huart->Init.Mode = UART_MODE_TX_RX;

    huart->Init.OverSampling = UART_OVERSAMPLING_16;

    huart->Init.HwFlowCtl = UART_HWCONTROL_NONE;

    if(HAL_UART_Init(huart) != HAL_OK)
    {
        print("Uart hal driver init failed.");
        return;
    }

    __HAL_UART_ENABLE_IT(huart,UART_IT_RXNE);
    __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_RXNE);
    __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_TC);

}

void user_uart_init(ze_u8_t id, uart_t* uart, ze_u32_t baudrate)
{
    GPIO_InitTypeDef  GPIO_InitStruct;
    if (id == UART_0)
    {
        UART0_CLK_ENABLE();
        UART0_CLK_SOURCE();
        UART0_GPIO_CLK_ENABLE();
        uart->flags = UART_EVENT_IRQ_RX;
        uart->private_data = &stm_uart0;
        stm_uart0.uart = uart;
        user_uart_config_rxfifo(uart, &uart0_rx_fifo, uart0_rxbuf, sizeof(uart0_rxbuf));
        /*##-2- Configure peripheral GPIO ##########################################*/
        /* UART TX GPIO pin configuration  */
        GPIO_InitStruct.Pin       = UART0_TX_PIN;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_PULLUP;
        GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;

        HAL_GPIO_Init(UART0_TX_GPIO_PORT, &GPIO_InitStruct);

        /* UART RX GPIO pin configuration  */
        GPIO_InitStruct.Pin = UART0_RX_PIN;
        GPIO_InitStruct.Mode    = GPIO_MODE_INPUT;
        GPIO_InitStruct.Pull    = GPIO_NOPULL;
        HAL_GPIO_Init(UART0_RX_GPIO_PORT, &GPIO_InitStruct);
        NVIC_SetPriority(stm_uart0.irq, 0);
        NVIC_EnableIRQ(stm_uart0.irq);
        stm_uart_init(uart, &stm_uart0, baudrate);

    }
}

void UART0_INTERRUPT(void)
{
    uart_hw_isr(stm_uart0.uart, UART_EVENT_IRQ_RX);
}


