#ifndef __USER_I2C_H__
#define __USER_I2C_H__
#include <bus/i2c.h>

typedef enum {
    I2C0 = 0,
    I2C1,
} i2c_name_t;

void user_i2c_init(i2c_name_t i2c, i2c_t* obj);

#endif